import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# Configuración del servidor SMTP
smtp_server = 'smtp.gmail.com'
smtp_port = 587

# TODO: Cambiar credenciales. No se puede usar el password general de gmail, sino que hay que generar una contraseña de app. Para más info, aquí
# https://recursospython.com/guias-y-manuales/enviar-correo-electronico-via-gmail-y-smtp/

# Credenciales del remitente
sender_email = 'tu_email@gmail.com'
sender_password = 'password'

# TODO: rellenar info del destinatario
# Información del destinatario
receiver_email = 'destinatario@gmail.com'
message_subject = 'Test envio correo'

# Creación del mensaje
message = MIMEMultipart()
message['From'] = sender_email
message['To'] = receiver_email
message['Subject'] = message_subject
message_body = 'Cuerpo del mensaje'
message.attach(MIMEText(message_body, 'plain'))

# Envío del correo electrónico
with smtplib.SMTP(smtp_server, smtp_port) as server:
    server.starttls()
    server.login(sender_email, sender_password)
    server.sendmail(sender_email, receiver_email, message.as_string())
