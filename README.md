
## Material necesario
- Sensor digital de temperatura y humedad DHT11 [aliexpres](https://es.aliexpress.com/item/1005001652349506.html?gatewayAdapt=glo2esp&spm=a2g0o.order_list.0.0.21ef194dkjwLR0)
- Raspberry pi (cualquier modelo, aunque yo he usado una zero W)
  
## Cuenta en thingspeak
- Crear una cuenta en [thingspeak](https://thingspeak.com/) y crear un canal

## Conexión del sensor
Conectamos el DHT11 de la siguiente forma (marcados en rojo en la imagen)
```
VCC: 5V 
GND: GND
DATA: GPIO4
```
![GPIO pins](images/rpi_gpio.png)
## Instalación del script

Desde un terminal de linea de comandos, conectamos a nuestra raspberry por ssh:
```
ssh admin@192.168.x.x
```

Descargamos el repositorio en nuestro directorio home (sustituir el nombre de usuario por el vuestro):
```
git clone https://usuario@bitbucket.org/fgbernal/temp_register.git
cd temp_register
```

Instalar dependencias:
```
pip install -r requirements.txt
```

Editamos el archivo `temp_register.py' y modificamos la KEY de nuestro canal de thingspeak.
```
KEY = "xxxxxxxxxxxxx"
```

El canal de thingspeak lo debemos haber configurado con dos campos con los siguientes nombres:
```    
'field1' (para la temperatura)
'field2' (para la humedad)
```

Ahora vamos a crear un servicio para poder ejecutar cómodamente el script y también hacer que se ponga en marcha automáticamente al iniciar el sistema

Copiamos el archivo temp_register.service en la carpeta `/lib/systemd/system/`
```
sudo cp ./temp_register.service /lib/systemd/system/temp_register.service
```

Actualizamos los permisos
```
sudo chmod 644 /lib/systemd/system/temp_register.service
chmod +x ./temp_register.py
```

Iniciamos el servicio
```
sudo systemctl daemon-reload
sudo systemctl enable temp_register.service
sudo systemctl start temp_register.service
```