# -*- coding: utf-8 -*-
import time
import logging
import argparse
import http.client
import urllib.request
import urllib.parse
import urllib.error
import Adafruit_DHT
import os

file_path = os.path.abspath(__file__)
base_dir = os.path.dirname(file_path)

# constants
URL = "api.thingspeak.com:80"
KEY = "xxxxxxxxxxxxxxx"

sensor_args = {'11': Adafruit_DHT.DHT11,
               '22': Adafruit_DHT.DHT22,
               '2302': Adafruit_DHT.AM2302}
sensor = sensor_args['11']
pin = 4

# Parse command line parameters.
parser = argparse.ArgumentParser(
    description="Toma datos de un sensor DHT11 y los publica en un canal de thingspeak")

parser.add_argument("-o", "--output",
                    help="nombre del archivo de log",
                    type=str,
                    default="temp_register.log")

parser.add_argument("-f", "--frequency",
                    help="frecuencia de muestreo en segundos",
                    type=int,
                    default=60)

args = parser.parse_args()

frecuency = float(args.frequency)

# Config msg log
logging.basicConfig(
    filename=os.path.join(base_dir, args.output), encoding="utf-8", level=logging.DEBUG, format="%(asctime)s: %(levelname)s: %(message)s"
)

# auxiliar functions


def read_temperature_and_humidity():
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
    logging.info("%s °C, %s hum." % (str(temperature), str(humidity)))
    return (str(humidity), str(temperature))

# report to thingspeak channel


def report_data_remotely(temperature, humidity):
    params = urllib.parse.urlencode(
        {'field1': temperature, 'field2': humidity, 'key': KEY})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    conn = http.client.HTTPConnection(URL)
    try:
        conn.request("POST", "/update", params, headers)
        response = conn.getresponse()
        logging.info(str(response.status) + " " + response.reason)
    except Exception as e:
        logging.error(e)
    conn.close()


if __name__ == '__main__':
    while True:
        humidity, temperature = read_temperature_and_humidity()
        report_data_remotely(temperature, humidity)
        time.sleep(frecuency)
