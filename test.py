# -*- coding: utf-8 -*-
import Adafruit_DHT

humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
print("Temp: %s °C" % temperature)
print("Hum: %s" % humidity)